# Business Search Back End

## Table of contents
* [General info](#general-info)
* [Technologies](#technologies)
* [Setup](#setup)
* [API examples](#api-examples)

## General info
This project is the Business Search Back End.
It exposes a API to search for businesses. 
	
## Technologies
Project is created with:
* Node.js
	
## Setup
To run this project, install it locally using npm:

```
git clone https://gitlab.com/business-search-code-challenge-mms/business-search-back-end.git
cd ./business-search-back-end
cp template.env .env
```
Add Yelp Fusion API Key to .env
```
npm install
node app
```
## API examples
Get business search by term, location and number of results wanted
```
http://localhost:3050/business/search?term=BBQ&location=Naperville, Illinois&nbResults=1
```
Result
```
{
    "businesses": [
        {
            "id": "hqf-yY6YW6EePk44IonUDg",
            "alias": "q-bbq-naperville",
            "name": "Q-BBQ",
            "image_url": "https://s3-media3.fl.yelpcdn.com/bphoto/qPN1A8h1WAH-xN1YVz-jWg/o.jpg",
            "is_closed": false,
            "url": "https://www.yelp.com/biz/q-bbq-naperville?adjust_creative=Rt1jhmrWSu1gVxrTQJwyNg&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=Rt1jhmrWSu1gVxrTQJwyNg",
            "review_count": 513,
            "categories": [
                {
                    "alias": "bbq",
                    "title": "Barbeque"
                }
            ],
            "rating": 4,
            "coordinates": {
                "latitude": 41.773975005027,
                "longitude": -88.149603978418
            },
            "transactions": [
                "pickup",
                "delivery"
            ],
            "price": "$$",
            "location": {
                "address1": "103 S Main St",
                "address2": "",
                "address3": "",
                "city": "Naperville",
                "zip_code": "60540",
                "country": "US",
                "state": "IL",
                "display_address": [
                    "103 S Main St",
                    "Naperville, IL 60540"
                ]
            },
            "phone": "+16306376400",
            "display_phone": "(630) 637-6400",
            "distance": 2965.271168365735
        }
    ]
}
```
Get business by alias
```
http://localhost:3050/business?alias=q-bbq-naperville
```
Result:
```
{
    "id": "hqf-yY6YW6EePk44IonUDg",
    "alias": "q-bbq-naperville",
    "name": "Q-BBQ",
    "image_url": "https://s3-media3.fl.yelpcdn.com/bphoto/qPN1A8h1WAH-xN1YVz-jWg/o.jpg",
    "is_claimed": true,
    "is_closed": false,
    "url": "https://www.yelp.com/biz/q-bbq-naperville?adjust_creative=Rt1jhmrWSu1gVxrTQJwyNg&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_lookup&utm_source=Rt1jhmrWSu1gVxrTQJwyNg",
    "phone": "+16306376400",
    "display_phone": "(630) 637-6400",
    "review_count": 513,
    "categories": [
        {
            "alias": "bbq",
            "title": "Barbeque"
        }
    ],
    "rating": 4,
    "location": {
        "address1": "103 S Main St",
        "address2": "",
        "address3": "",
        "city": "Naperville",
        "zip_code": "60540",
        "country": "US",
        "state": "IL",
        "display_address": [
            "103 S Main St",
            "Naperville, IL 60540"
        ],
        "cross_streets": ""
    },
    "coordinates": {
        "latitude": 41.773975005027,
        "longitude": -88.149603978418
    },
    "photos": [
        "https://s3-media3.fl.yelpcdn.com/bphoto/qPN1A8h1WAH-xN1YVz-jWg/o.jpg",
        "https://s3-media2.fl.yelpcdn.com/bphoto/Y9eSNOQFmxiKKHt8lEJDJg/o.jpg",
        "https://s3-media1.fl.yelpcdn.com/bphoto/Oac2DmwfoECA4jtZRINNUQ/o.jpg"
    ],
    "price": "$$",
    "hours": [
        {
            "open": [
                {
                    "is_overnight": false,
                    "start": "1100",
                    "end": "2100",
                    "day": 0
                },
                {
                    "is_overnight": false,
                    "start": "1100",
                    "end": "2100",
                    "day": 1
                },
                {
                    "is_overnight": false,
                    "start": "1100",
                    "end": "2100",
                    "day": 2
                },
                {
                    "is_overnight": false,
                    "start": "1100",
                    "end": "2100",
                    "day": 3
                },
                {
                    "is_overnight": false,
                    "start": "1100",
                    "end": "2100",
                    "day": 4
                },
                {
                    "is_overnight": false,
                    "start": "1100",
                    "end": "2100",
                    "day": 5
                },
                {
                    "is_overnight": false,
                    "start": "1100",
                    "end": "2100",
                    "day": 6
                }
            ],
            "hours_type": "REGULAR",
            "is_open_now": false
        }
    ],
    "transactions": [
        "pickup",
        "delivery"
    ]
}
```
Get business reviews by alias
```
http://localhost:3050/business/reviews?alias=q-bbq-naperville
```
Result:
```
{
    "reviews": [
        {
            "id": "yrJtaxOBA0zyjP3y_YHXUw",
            "url": "https://www.yelp.com/biz/q-bbq-naperville?adjust_creative=Rt1jhmrWSu1gVxrTQJwyNg&hrid=yrJtaxOBA0zyjP3y_YHXUw&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_reviews&utm_source=Rt1jhmrWSu1gVxrTQJwyNg",
            "text": "Ordered delivery for lunch with parents. They both really enjoyed their corned beef and brisket sandwiches. I opted for the cod tacos..was a little...",
            "rating": 4,
            "time_created": "2021-03-09 12:24:49",
            "user": {
                "id": "3zqh-j_bTGkZHHdejWZ5ug",
                "profile_url": "https://www.yelp.com/user_details?userid=3zqh-j_bTGkZHHdejWZ5ug",
                "image_url": null,
                "name": "Dominique M."
            }
        },
        {
            "id": "fUH64r_u--ETwpWAyz7PGw",
            "url": "https://www.yelp.com/biz/q-bbq-naperville?adjust_creative=Rt1jhmrWSu1gVxrTQJwyNg&hrid=fUH64r_u--ETwpWAyz7PGw&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_reviews&utm_source=Rt1jhmrWSu1gVxrTQJwyNg",
            "text": "Had takeout the other day and tried the Lil Q platter and smoked spicy wings. Everything was delicious except the sliced brisket which was very dry and...",
            "rating": 4,
            "time_created": "2020-11-03 22:10:06",
            "user": {
                "id": "cHLH9vYz35f4OhSQ79Y2sA",
                "profile_url": "https://www.yelp.com/user_details?userid=cHLH9vYz35f4OhSQ79Y2sA",
                "image_url": null,
                "name": "James D."
            }
        },
        {
            "id": "1v70r2DKMN7hJVQcQdp-vw",
            "url": "https://www.yelp.com/biz/q-bbq-naperville?adjust_creative=Rt1jhmrWSu1gVxrTQJwyNg&hrid=1v70r2DKMN7hJVQcQdp-vw&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_reviews&utm_source=Rt1jhmrWSu1gVxrTQJwyNg",
            "text": "Great food . Highly suggest the platter if you have 2-3 people . I am impress .they also have a nice outdoor seating",
            "rating": 5,
            "time_created": "2020-09-21 13:02:47",
            "user": {
                "id": "VpNzWWqw_ROORm-Y3aFhyw",
                "profile_url": "https://www.yelp.com/user_details?userid=VpNzWWqw_ROORm-Y3aFhyw",
                "image_url": null,
                "name": "Peggy P."
            }
        }
    ],
    "total": 513,
    "possible_languages": [
        "en"
    ]
}
```
