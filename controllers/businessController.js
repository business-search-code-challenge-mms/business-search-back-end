const { yelpAPIKey } = require('../config')

const yelp = require('yelp-fusion')
const client = yelp.client(yelpAPIKey)


exports.getBusinessSearch = function(req, res) {
    let term = req.query.term
    let location = req.query.location 
    let nbResults = req.query.nbResults
    
    client.search({
        term: term,
        location: location
    }).then(response => {
        businesses = []
        if (response.statusCode !== 200){
          res.send({businesses: businesses})
        } else {

          for(i = 0; i < nbResults; i++){
            if(response.jsonBody.businesses[i]){
                businesses[i] = response.jsonBody.businesses[i];
            }
        }
        res.send({businesses: businesses})
        }
        
      }).catch(e => {
        console.log(e)
      });
}

exports.getBusiness = function(req, res) {
    let alias = req.query.alias
    client.business(alias).then(response => {

      if (response.statusCode !== 200){
        res.send({business: {}})
      }else{
        res.send({business: response.jsonBody})
      }
      }).catch(e => {
        console.log(e)
      })
}

exports.getBusinessReviewsById = function(req, res) {
    let alias = req.query.alias
    client.reviews(alias).then(response => {
      if (response.statusCode !== 200){
        res.send({reviews:{}})
      }else{
        res.send(response.jsonBody)
      }
      }).catch(e => {
        console.log(e)
      });
}