const dotenv = require('dotenv')
dotenv.config()

module.exports = {
    port: process.env.PORT,
    yelpAPIKey: process.env.YELP_API_KEY,
  }