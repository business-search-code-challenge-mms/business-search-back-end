var express = require('express') 
var business_router = express.Router() 

var businessController = require('../controllers/businessController')


business_router.get('/business/search', businessController.getBusinessSearch)

business_router.get('/business', businessController.getBusiness)

business_router.get('/business/reviews', businessController.getBusinessReviewsById)


module.exports = business_router