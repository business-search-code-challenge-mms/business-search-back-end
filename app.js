let express = require('express')
let app = express()

let cors = require('cors')
app.use(cors())
const { port } = require('./config')

let businessRoute = require('./routers/business_routes')
app.use(businessRoute)
app.use(express.static('public'))


app.listen(port,()=> console.info(`Server has started on ${port}`))